# Maintainer:  Iyán Méndez Veiga <me (at) iyanmv (dot) com>
# Contributor: Shaun Ren <shaun DOT ren (at) linux (dOt) com>
# Contributor: Simon Hanna <simon DOT hanna (at) serve-me (dOt) info>

pkgname=rtl88xxau-aircrack-dkms-git
_pkgbase=rtl88xxau
pkgver=r1201
pkgrel=3
pkgdesc="Aircrack-ng kernel module for Realtek 88XXau (USB adapters only) network cards (8811au, 8812au and 8821au chipsets) with monitor mode and injection support"
url="https://github.com/aircrack-ng/rtl8812au"
license=('GPL')
arch=('aarch64' 'x86_64')
makedepends=('git')
conflicts=('rtl8812au-aircrack-dkms-git' 'rtl8812au-dkms-git' 'rtl8821au-dkms-git' 'rtl8814au-dkms-git' 'rtl8812au-inject-dkms-git')
provides=("${_pkgbase}=${pkgver%%.*}")

source=('rtl88xxau::git+https://github.com/aircrack-ng/rtl8812au.git#branch=v5.6.4.2'
        'dkms.conf'
        'linux60.patch' 'linux519.patch')

sha256sums=('SKIP'
            '1ac36b3713d6b719b8c5f820653bbe30ead8323165d52e6ad95d56da627fde88'
            '30357dabfbfd6b21112dcd4f6e4acae9b0267f9c92bade06883fbb5dd20933d1'
            '7eaff35aed498685c6aa6be64cc24f5df581b52a35d69aebac8370e5daa4ec42')

pkgver() {
    cd "${srcdir}/${_pkgbase}"
    ( set -o pipefail
    git describe --long 2>/dev/null | sed 's/\([^-]*-g\)/r\1/;s/-/./g' ||
    printf "r%s" "$(git rev-list --count HEAD)"
    )
}

prepare() {
    cd "${srcdir}/${_pkgbase}"
    patch -p1 -i ${srcdir}/linux60.patch
    patch -p1 -i ${srcdir}/linux519.patch

    if [ "${CARCH}" = "aarch64" ]; then
        sed -i 's/CONFIG_PLATFORM_I386_PC = y/CONFIG_PLATFORM_I386_PC = n/g' Makefile
        sed -i 's/CONFIG_PLATFORM_ARM64_RPI = n/CONFIG_PLATFORM_ARM64_RPI = y/g' Makefile
    fi
}

package() {
    depends=('dkms' 'linux-headers')
    cd "${srcdir}/${_pkgbase}"
    mkdir -p "${pkgdir}/usr/src/${_pkgbase}-${pkgver}"
    cp -pr * "${pkgdir}/usr/src/${_pkgbase}-${pkgver}"
    cp ${srcdir}/dkms.conf "${pkgdir}/usr/src/${_pkgbase}-${pkgver}"

    if [ "${CARCH}" = "aarch64" ]; then
        sed -i 's/^MAKE="/MAKE="ARCH=arm64\ /' "${pkgdir}/usr/src/${_pkgbase}-${pkgver}/dkms.conf"
    fi

    # Set name and version
    sed -e "s/@_PKGBASE@/${_pkgbase}-dkms/" \
        -e "s/@PKGVER@/${pkgver}/" \
    -i "${pkgdir}"/usr/src/${_pkgbase}-${pkgver}/dkms.conf
}
